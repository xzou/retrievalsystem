import ch.ethz.dal.tinyir.processing.TipsterCorpusIterator
import ch.ethz.dal.tinyir.processing.TipsterParse
import java.util.HashMap
import java.util.ArrayList
import java.util.Comparator
import java.util.PriorityQueue
import java.io.PrintWriter
import scala.math.log
import scala.util.Sorting
import sun.misc.Resource

class termModel(querys: Array[topic], docPath: String)
{
  // number of documents per query
  var n = 100
  
  // initializing document frequency
  val words = querys.flatMap { x => x.query }.distinct
  val wordsCount = new HashMap[String, Int]()
  words.foreach { x => wordsCount.put(x, 0) }
  //words.foreach(println)
  
  // initializing term frequnce
  val comparator = new tfComparator()
  val termsCount = new Array[PriorityQueue[termFreq]](querys.length)
  for(i <- 0 until termsCount.length){
    termsCount(i) = new PriorityQueue[termFreq](comparator)
  }
  
  // computing df in the first run
  var docCount = 0.0;
  var docIt = new TipsterCorpusIterator(docPath)
  while(docIt.hasNext){
    val doc = docIt.next
    val tokens = doc.tokens
    // calculate document frequency
	  for(word: String <- words if tokens.contains(word)){
	    //if(wordsCount.get(word).toDouble > docCount){
	    //  println(wordsCount.get(word) + " " + docCount)
	    //}
	    //wordsCount.put(word, wordsCount.get(word)+1)
	    wordsCount.put(word, wordsCount.get(word)+1)
	  }
    docCount = docCount + 1.0
    println(docCount)
  }
  
  // extract features
  //val feature = new PrintWriter("tf.txt")
  
  // computing tf in the second run
  docIt = new TipsterCorpusIterator(docPath)
  while(docIt.hasNext){  	
    val doc = docIt.next
	  val tokens = doc.tokens
	  
	  for(i <- 0 until querys.length){
	    //println(querys(i).query.map(x => tokens.count(_ == x)))
	    val tf = new termFreq(doc.name, querys(i).query.map(x => tokens.count(_ == x)))
	    tf.calc(wordsCount, querys(i).query, docCount)
	    termsCount(i).add(tf)
	    if(termsCount(i).size > n){
	      termsCount(i).remove()
	    }
	    
	    //feature.write(querys(i).id + " " + doc.name + " " + tf.score + "\n")
	  }
	}
  
  //feature.close
  
  /* rank the scores for each query 
  for(i <- 0 until termsCount.length){
    for(j <- 0 until termsCount(i).length){
      termsCount(i)(j).calc(wordsCount, querys(i).query, docCount)
    }
    Sorting.quickSort(termsCount(i))
    //termsCount(i) = termsCount(i).sortWith((e1, e2) => (e1.score >= e2.score))
  } */
  
  def result(){
    for(i <- 0 until termsCount.length){
      println(querys(i).id)
      println(querys(i).query)
      var res = new ArrayList[termFreq](0)
      while(!termsCount(i).isEmpty()){
        val tf = termsCount(i).peek()
        res.add(tf)
        termsCount(i).remove()
      }
      println(res.size)
      var j = res.size-1
      while(j >= 0){
        println(res.get(j).name)
        println(res.get(j).score)
        println(res.get(j).count)
        j -= 1
      }
    }
  }
  
  def output(){
    val output = new PrintWriter("ranking-t-28.run")
    for(i <- 0 until termsCount.length){
      var res = new ArrayList[termFreq](0)
      while(!termsCount(i).isEmpty()){
        val tf = termsCount(i).peek()
        res.add(tf)
        termsCount(i).remove()
      }
      var j = res.size-1
      while(j >= 0){
        output.write(querys(i).id+" ")
        output.write((n - j) + " ")
        output.write(res.get(j).name.replaceAll("-", "") + "\n")
        j -= 1
      }
    }
    output.close()
  }
}

class termFreq(id: String, tf: List[Int]) extends Ordered[termFreq]
{
  val name = id
  val count = tf
  var score = 0.0
  
  def calc(df: HashMap[String, Int], words: List[String], n: Double){
    val sum = count.sum.toDouble
    for(i <- 0 until words.length){
      score += log(1.0+count(i).toDouble)*log((n+1.0)/(df.get(words(i)).toDouble+1.0))
      //if(log(1.0+count(i).toDouble)*log((n+1.0)/(df.get(words(i)).toDouble+1.0)) < 0)
      //  println(count(i).toDouble + " " + n + " " + df.get(words(i)).toDouble)
    }
  }
  
  def compare(that: termFreq) = {
    val dif = this.score - that.score
    if(dif > 0) -1
    else if(dif < 0) 1
    else 0
  }
}

class tfComparator extends Comparator[termFreq]{
  def compare(x: termFreq, y:termFreq) = {
    val dif = x.score - y.score
    if(dif > 0) 1
    else if(dif < 0) -1
    else 0
  }
}

