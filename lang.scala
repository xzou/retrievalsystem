import ch.ethz.dal.tinyir.processing.TipsterCorpusIterator
import ch.ethz.dal.tinyir.processing.TipsterParse
import java.util.HashMap
import java.util.ArrayList
import java.util.Comparator
import java.util.PriorityQueue
import java.io.PrintWriter
import scala.math.log
import scala.util.Sorting
import sun.misc.Resource

class langModel(querys: Array[topic], docPath: String)
{
  // number of documents per query
  var n = 100
  
  // initializing collection frequency
  var collectionLength = 0.0
  var maxDocumentLength = 0
  val words = querys.flatMap { x => x.query }.distinct
  val wordsCount = new HashMap[String, Int]()
  words.foreach { x => wordsCount.put(x, 0) }
  
  // initializing term frequnce
  val comparator = new laComparator()
  val termsCount = new Array[PriorityQueue[langFreq]](querys.length)
  for(i <- 0 until termsCount.length){
    termsCount(i) = new PriorityQueue[langFreq](comparator)
  }
  
  // computing cf in the first run
  var docCount = 0;
  var docIt = new TipsterCorpusIterator(docPath)
  while(docIt.hasNext){
    val doc = docIt.next
    val tokens = doc.tokens
    
    collectionLength += tokens.length.toDouble
    if(tokens.length > maxDocumentLength)
      maxDocumentLength = tokens.length
      
    // calculate collection frequency
	  for(word: String <- words){
	    wordsCount.put(word, wordsCount.get(word)+tokens.count { x => x == word })
	  }
    docCount = docCount + 1
    println(docCount)
  }
  
  // extract feature
  //val feature = new PrintWriter("la.txt") 
  
  // computing tf in the second run
  docIt = new TipsterCorpusIterator(docPath)
  while(docIt.hasNext){  	
    val doc = docIt.next
	  val tokens = doc.tokens
	  
	  for(i <- 0 until querys.length){
	    //println(querys(i).query.map(x => tokens.count(_ == x)))
	    val tf = new langFreq(doc.name, querys(i).query.map(x => tokens.count(_ == x)))
	    tf.calc(wordsCount, querys(i).query, tokens.length, maxDocumentLength, collectionLength)
	    termsCount(i).add(tf)
	    if(termsCount(i).size > n){
	      termsCount(i).remove()
	    }
	    
	    //feature.write(querys(i).id + " " + doc.name + " " + tf.score + "\n")
	  }
	}
  
  //feature.close
  
  def result(){
    for(i <- 0 until termsCount.length){
      println(querys(i).id)
      println(querys(i).query)
      var res = new ArrayList[langFreq](0)
      while(!termsCount(i).isEmpty()){
        val la = termsCount(i).peek()
        res.add(la)
        termsCount(i).remove()
      }
      var j = res.size-1
      while(j >= 0){
        println(res.get(j).name)
        println(res.get(j).score)
        println(res.get(j).count)
        j -= 1
      }
    }
    println(collectionLength)
    println(maxDocumentLength)
  }
  
  def output(){
    val output = new PrintWriter("ranking-l-28.run")
    for(i <- 0 until termsCount.length){
      var res = new ArrayList[langFreq](0)
      while(!termsCount(i).isEmpty()){
        val la = termsCount(i).peek()
        res.add(la)
        termsCount(i).remove()
      }
      var j = res.size-1
      while(j >= 0){
        output.write(querys(i).id+" ")
        output.write((n - j) + " ")
        output.write(res.get(j).name.replaceAll("-", "") + "\n")
        j -= 1
      }
    }
    output.close()
  }
}

class langFreq(id: String, tf: List[Int]) extends Ordered[langFreq]
{
  val name = id
  val count = tf
  var score = 0.0
  
  def calc(cf: HashMap[String, Int], words: List[String], n: Int, maxn: Int, m: Double){
    val lambda = 0.25*(1.0-n.toDouble/maxn.toDouble)
    for(i <- 0 until words.length){
      if(count(i) > 0){
        score += log(1.0+(1.0-lambda)*count(i).toDouble*m/n.toDouble/lambda/cf.get(words(i)).toDouble)
        //score += log((1.0-lambda)*count(i).toDouble/n.toDouble + lambda*cf.get(words(i)).toDouble/m.toDouble)
      }
    }
    score += log(1.0 + lambda)
  }
  
  def compare(that: langFreq) = {
    val dif = this.score - that.score
    if(dif > 0) -1
    else if(dif < 0) 1
    else 0
  }
}

class laComparator extends Comparator[langFreq]{
  def compare(x: langFreq, y:langFreq) = {
    val dif = x.score - y.score
    if(dif > 0) 1
    else if(dif < 0) -1
    else 0
  }
}