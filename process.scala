import java.util.HashMap
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.MutableList
import java.io.PrintWriter
import java.io.InputStreamReader
import java.io.FileInputStream
import java.io.BufferedReader

class processor(truthPath: String)
{
  val truthFile = scala.io.Source.fromFile(truthPath).mkString.split("[\n]").toList.map { x => x.split("[ ]").toList }
  
  var truth = new ArrayBuffer[HashMap[String, Boolean]](0)
  
  var num = 0
  var former = "0"
  for(piece: List[String] <- truthFile){
    if(piece(0) != former){
      truth += new HashMap[String, Boolean]
      num += 1
      former = piece(0)
    }
    val relate = if(piece(3) == "1") true else false
    truth(num-1).put(piece(2).replaceAll("[-]", ""), relate)
  }
  
  val result = new PrintWriter("/tmp/tipster/" + "feature.csv")
  result.write("f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, Label\n")
  
  var i = 1
  while(i < 41){
    var file = new FileInputStream("/tmp/tipster/"+(i+50)+".txt")
    var reader = new BufferedReader(new InputStreamReader(file))
    var line = reader.readLine()
    while(line != null){
      //println(line)
      val values = line.split("[ ]")
      if(truth(i-1).containsKey(values(1))){
        val relate = truth(i-1).get(values(1))
        for(j <- 2 to 11){
          result.write(values(j) + ", ")
        }
        if(relate){
          result.write("Rel" + "\n")
        }
        else{
          result.write("Inr" + "\n")
        }
      }
      line = reader.readLine()
    }
    reader.close()
    file.close()
    i = i + 1
  }
  
  result.close
}
