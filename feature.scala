import ch.ethz.dal.tinyir.processing.TipsterCorpusIterator
import ch.ethz.dal.tinyir.processing.TipsterParse
import java.util.HashMap
import java.util.ArrayList
import java.util.Comparator
import java.util.PriorityQueue
import java.io.PrintWriter
import scala.math.log
import scala.util.Sorting
import sun.misc.Resource

class feature(name: String, count: List[Double])
{
  val id = name
  val tf = count
  var f0 = 0.0
  var f1 = 0.0
  var f2 = 0.0
  var f3 = 0.0
  var f4 = 0.0
  var f5 = 0.0
  var f6 = 0.0
  var f7 = 0.0
  var f8 = 0.0
  var f9 = 0.0
  var score = 0.0
  
  def calc(d: HashMap[String, Double], c: HashMap[String, Double], words: List[String], docNum: Double, docLength: Double, tokNum: Double){
    for(i <- 0 until words.length){
      var df = d.get(words(i))
      var cf = c.get(words(i))
      f0 += count(i)
      f1 += log(count(i) + 1.0)
      f2 += (count(i)+1.0)/(docLength+1.0)
      f3 += log((count(i)+1.0)/(docLength+1.0) + 1.0)
      f4 += log((docNum+1.0)/(df+1.0))
      f5 += log(log((docNum+1.0)/(df+1.0)))
      f6 += log((tokNum+1.0)/(cf+1.0) + 1.0)
      f7 += log((count(i)+1.0)/(docLength+1.0)*log((docNum+1.0)/(df+1.0)) + 1.0)
      f8 += count(i)*log((docNum+1.0)/(df+1.0))
      f9 += log((count(i)+1.0)/(docLength+1.0)*(tokNum+1.0)/(cf+1.0) + 1.0)
    }
    //score = 0.0206*f1+0.5687*f3-0.0106*f4+0.0507*f5-0.0089*f6-0.2626*f7-0.0001*f8+0.0145*f9+0.2082
    score = -0.0206*f1-0.5687*f3+0.0106*f4-0.0507*f5+0.0089*f6+0.2626*f7+0.0001*f8-0.0145*f9+0.7908
  }
  
  def write(name: String, writer: PrintWriter){
    writer.write(name + " " + id + " " + f0 + " " + f1 + " " + f2 + " " + f3 + " " + f4 + " " + f5 + " " + f6 + " " + f7 + " " + f8 + " " + f9 + "\n")
  }
}

class extractor(querys: Array[topic], docPath: String)
{ 
  // initializing document frequency and collection frequency
  val terms = querys.flatMap { x => x.query }.distinct
  val termsDF = new HashMap[String, Double]()
  val termsCF = new HashMap[String, Double]()
  terms.foreach { x => termsDF.put(x, 0.0) }
  terms.foreach { x => termsCF.put(x, 0.0) }
  
  // initializing term frequnce
  /*val writers = new Array[PrintWriter](querys.length)
  for(i <- 0 until writers.length){
    writers(i) = new PrintWriter("/tmp/tipster/" + querys(i).id + ".txt")
  } */
  
  // computing df and cf in the first run
  var docCount = 0.0;
  var tokCount = 0.0
  var docIt = new TipsterCorpusIterator(docPath)
  while(docIt.hasNext){
    val doc = docIt.next
    val tokens = doc.tokens
    
    // calculate document and collection frequence
	  for(word: String <- terms if tokens.contains(word)){
	    termsDF.put(word, termsDF.get(word)+1.0)
	    termsCF.put(word, termsCF.get(word)+tokens.count { x => x == word }.toDouble)
	  }
    docCount = docCount + 1.0
    tokCount = tokCount + tokens.length
    println(docCount)
  }
  
  val comparator = new fComparator()
  val docScores = new Array[PriorityQueue[feature]](querys.length)
  for(i <- 0 until docScores.length){
    docScores(i) = new PriorityQueue[feature](comparator)
  }
  
  // computing tf in the second run
  docIt = new TipsterCorpusIterator(docPath)
  while(docIt.hasNext){  	
    val doc = docIt.next
	  val tokens = doc.tokens
	  
	  for(i <- 0 until querys.length){
	    val f = new feature(doc.name, querys(i).query.map(x => tokens.count(_ == x).toDouble))
	    f.calc(termsDF, termsCF, querys(i).query, docCount, tokens.length.toDouble, tokCount)
	    //f.write(querys(i).id.toString, writers(i))
	    docScores(i).add(f)
	    if(docScores(i).size > 100){
	      docScores(i).remove()
	    }
	  }
	}
  
  /*for(i <- 0 until writers.length){
    writers(i).close()
  }*/
  
  def output(){
    val output = new PrintWriter("ranking-r-28.run")
    for(i <- 0 until docScores.length){
      var res = new ArrayList[feature](0)
      while(!docScores(i).isEmpty()){
        val tf = docScores(i).peek()
        res.add(tf)
        docScores(i).remove()
      }
      var j = res.size-1
      while(j >= 0){
        output.write(querys(i).id+" ")
        output.write((100 - j) + " ")
        output.write(res.get(j).id.replaceAll("-", "") + "\n")
        //output.write(res.get(j).score.toString)
        j -= 1
      }
    }
    output.close()
  }
}

class fComparator extends Comparator[feature]{
  def compare(x: feature, y:feature) = {
    val dif = x.score - y.score
    if(dif > 0) 1
    else if(dif < 0) -1
    else 0
  }
}

