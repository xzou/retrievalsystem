import ch.ethz.dal.tinyir.processing.Tokenizer

class topic(number: String, content: String)
{
  var id = number.trim.toInt
  val query = Tokenizer.tokenize(content.trim).filter(_ != "")
}

class topicReader(path: String)
{
  var list = new Array[topic](0)
  
  def read(){
    val file = scala.io.Source.fromFile(path).mkString
    var num = file.indexOf("<num>")
    var end = file.indexOf("\n", num)
    
    while(num != -1){
      num = file.indexOf(":", num)
      val number = file.substring(num+1, end)
      num = file.indexOf("<title>", end)
      end = file.indexOf("\n", num)
      num = file.indexOf(":", num)
      val content = file.substring(num+1, end)
      list = list :+ new topic(number, content)
      num = file.indexOf("<num>", end)
      end = file.indexOf("\n", num)
    }
    
    //this.check()
  }
  
  def check(){
    list.foreach( item =>{
        println(item.id)
        println(item.query)
    })
  }
}